# Bootstrap Minimal

This is a sub-theme using Bootstrap as its base theme, so consider installing Bootstrap as well if you would like to use this theme. The theme is clean and minimalist and still keeps the Bootstrap look to it. The theme also adds some nice buttons for articles, log in, register, etc and adds some cool hover effects for them.

## Installation instructions

1. Place this theme directory in [your-drupal-directory]/themes/custom
2. Enable the theme at example.com/admin/appearance

## Configuration

The theme settings can be configured as per preference at example.com/admin/appearance/settings/bootstrap_minimal
